label creditos:
    $ velocidad_creditos = 25
    scene creditos_Finales #crear una escena
    show creditos_img at Move((0.5,1.0), (0.5, -1.0), velocidad_creditos,xanchor = 0.5, yanchor = 0)
    with Pause(velocidad_creditos+10)

#     Step 1 – Grab an Image Editor
# The first thing I did was open up an image editor, my preferred weapon of choice is the free and powerful GIMP.
#
# I created a credits image with a transparent background, adding the text to the image exactly where and how I wanted it displayed.
#
# This had the added benefit of allowing me the option of embedding images and stylistic designs into my credits, though I ran out of time to do so.
#
# Step 2 – A Few Lines of Code
# Once I had my credits image saved in the images folder of my RenPy project, I simply had to create a credits label with the following four lines of codecode under it.
#
# label credits:
#     $ credits_speed = 25
#     scene black
#     show credits_image at Move((0.5, 1.0), (0.5, -1.0), credits_speed,
#                   xanchor=0.5, yanchor=0)
#     with Pause(credits_speed+10)
# That’s it!
#
# First we set a convenience variable called “credits_speed” which will be the time in seconds that it takes our credits to scroll.
#
# Then we turn the scene black.
#
# Then we load in our credits image (called “credits_image”) in the center, but just below the screen. Our credits begin automatically scrolling up once we reach this line of code.
#
# Lastly, we tell RenPy to wait for 10 seconds after the credits have scrolled.
