
label splashscreen:#TODO : Imagen mas estilizada o animacion !!
        scene p_oscura
        show text "R.S.S One Chimp Studio..." with fade
        $ renpy.pause(2.0)
        hide text with fade
        return

label start:


    stop music fadeout 2.0  ##detiene el tema principal
    pause 1.0

    play music music_amb fadein 2.0
    scene p_oscura
    """Todo surge de repente...
    el mundo es un lugar un poco confuso,

    realmente extenso, tangible.....""" ##cps transicion de texto {cps=x} x=numero si x=25 normal si x=40 rapido
    #with
    """este mundo es cruel......la naturaleza humana simplemente no entiende,
     ha causado diversas desastres, masacres, ha destruido ecosistemas solo con el afan de
     expandir sus horizontes, diversos lugares han sucumbido ante la humanidad."""
    """Se dieron cuenta de lo que habian hecho pero ya fue muy tarde, simplemente no
        les quedaba de otra que aceptar su destino.
        .........................................................................................."""
    "Que culpa tenia yo? al verme implicado solo por el hecho de existir....o era solo el destino"
    ".......... "
    #with dissolve >> tratar de englobar toda la conversacion con sola una linea de transiciones dissolve en el exterior
    stop music fadeout 2.0
    pause 1.0
    play music music_casual fadein 2.0

    show screen ctc #TODO englobar en una funcion!!
    show screen fecha("Fecha: 20/10/2025")
    show screen indicador
    #show screen achievementdisplay
    scene d1_inicio
    scene ciudad
    with blinds

    $ persistent.unlock_1 = True

    "vaya que tiempo"
    "Noche......es irreal de cierta manera "
    """muros de concreto a mi alrededor, pavimento, faros, automoviles
    ,hay diversidad de edificios realmente grandes y deslumbrantes
    puentes reflejando el agua de manera especial , un rio que refleja este anochecer ..
    casas, tiendas, mercados, oficinas"""
    "solo un poco de oscuridad en todo por asi decirlo.."
    "voy caminando a travez de ellos..ha pasado tiempo desde que recorri la ciudad."
    "Ciudad A:."
    "Una de las urbes tecnologicamente mas avanzadas"
    "muchas personas realmente vienen aqui a quedarse y llevar sus vidas con tranquilidad y trabajo"
    "se respira un aire de tranquilidad sumamente bueno.."
    dav "hah *que noche tan tranquila..."
    "*ahhh... mis pulmones se llenan lentamente............... exhalo e inhalo....es refrescante"
    dav "*pasos"
    "las calles han estado asi desde que he llegado "
    stop music fadeout 2.0

    play music music_tension fadein 2.0

    hide ciudad
    hide fecha
    hide indicador
    hide menuPers
    with dissolve

    nvlText "hace 55 años hubo un cataclismo mundial.."
    ##  parraf con tiempo
    nvlText""" =='********************************'==

        CIFRAS:
                3/4 partes de la poblacion total del planeta sucumbio.
                incluyendo casi  todas las especies de animales."""
    nvlText"""PEM (Pulso Electromagnetico): Daño gran parte de la tecnologia existente.
                Zonas Radioactivas: basicamente 'puntos muertos', nada sobrevive ahi.
                Desastres Naturales azotaron  los continentes..
                tsunamis, terremotos, mareas, derrumbes, toda una tragedia """
    nvlText"==  Se dice que estaba destinado a ocurrir.  =="
    hide nvltext with dissolve

    stop music fadeout 2.0
    $ get_achievement("Logro7", trans=achievement_transform)##muestra el logro desbloqueado
    play music music_casual fadein 2.0
    $ get_achievement("Logro2", trans=achievement_transform)##muestra el logro desbloqueado

    scene ciudad
    with dissolve
    "he pensado en eso ultimamente..."
    "cual fue la razon?, solamente la naturaleza nos dio nuestro merecido?"
    "solo hay un detalle"
    "Somos libres?"
    "a que me refiero..pues"
    "Basicamente hemos vivido confinados"
    "es una paz realmente escalofriante..."
    "me han contado que existen muchos lugares o existieron.."
    "solo eso me han dicho pero si lo piensas da mala espina"
    "zonas radioactivas"
    "son desconocidas"
    "se enviaron expediciones a lo largo de los años pero nunca volvieron"
    "el gobierno no ha dicho nada y realmente tambien nadie quiere  hacerlo"
    "razones............quien sabe"
    "ademas de que somos  monitoreados...."
    "todos los habitantes tenemos implantados una clase de chip o algo asi"
    "los cuales contienen nuestros datos personales y entre otros"
    "nadie esta autorizado a salir a otras ciudaddes sin llevar a cabo un riguroso papeleo"
    "que estupidez"
    "Las ciudades vecinas..talvez un dia salga"
    "...."
    "bien al parecer todavia no llega....oh una tienda  algun ,hum.. echare un vistazo no pierdo nada, en esta parte de la ciudad es raro"
    "encontrar una de estas tiendas con este estilo claramente..."
    "no hay muchas tiendas que realmente se han atraido por vender todo estos tipos de productos.."
    "puedes encontrar diversidad de articulos de todo tipo, algunos son realmente raros y aun asi se venden...el chiste es que todo el producto sea"
    "vendido y obtener dinero..."

    "pues que mas es obvio no?!!"

    "bien.."
    ## XXX: test del contrareloj!!
    #show screen quick_bar
    show contrarelojV2 at Position(xalign=0.5, yalign=0.5)
    $ ui.timer(10.0, ui.jumps("nodecidio"))
    menu:
            "Entrar":
                hide contrarelojV2
                jump decisionUno
            "No entrar":
                hide contrarelojV2
                jump decisionDos
label decisionUno: ## "ruta o decision agregar valores (boolean, if'[else], contadores, int, etc!)afectara el juego, depende de las variables"
    "Entrando al capitulo 1"
    jump capituloUno

label decisionDos:
    "Entrando al capitulo 2"
    jump capituloDos

label nodecidio:
    "Entrando al capitulo 3"
    jump capituloTres

##TODO scripts .rpy de finales aparte!! ##############################################################################################


    #$persistent.finalVerdadero= True
