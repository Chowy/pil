init:

## aniade personajes en el juego como en el ejemplo:
    define nvlText = Character("x",color="#ffffff", kind=nvl)
    define dav = Character("David",color="#9999FF")
    define mit = Character("Mitchell",color="ffb2b2")
    define nur = Character("Nuri",color="#ffc1ff")
    define dia = Character("Diana",color="#ffffbb")
    ## TODO: mas personajes!!

## posiciones personajes
    $ left = Position(xpos=0.0, xanchor='left')
    $ right = Position(xpos=1.0, xanchor='right')

    $ center = Position()
    $ wiperight = CropMove(1.0, "wiperight")
    $ wipeleft = CropMove(1.0, "wipeleft")
    $ wipeup = CropMove(1.0, "wipeup")
    $ wipedown = CropMove(1.0, "wipedown")

    $ slideright = CropMove(1.0, "slideright")
    $ slideleft = CropMove(1.0, "slideleft")
    $ slideup = CropMove(1.0, "slideup")
    $ slidedown = CropMove(1.0, "slidedown")

    $ slideawayright = CropMove(1.0, "slideawayright")
    $ slideawayleft = CropMove(1.0, "slideawayleft")
    $ slideawayup = CropMove(1.0, "slideawayup")
    $ slideawaydown = CropMove(1.0, "slideawaydown")

    $ irisout = CropMove(1.0, "irisout")
    $ irisin = CropMove(1.0, "irisin")

## Transiciones
    $ fade = Fade(.5, 0, .5) # Fade to black and back.
    $ dissolve = Dissolve(0.5)

## aniade las archivos .png, jpg de los personajes
    image david = "images/david/david.png"

                    #image miki normal = "images/Miki/m_Normal.png"
                    #   usar show >> para mostrar el personaje
                    #           show miki pensativa
                    #   usar hide >> para para ocultar el personaje
                    #           hide miki pensativa
                    #   at >> usado para indicar la posicion del personaje
                    #           show miki pensativa at right

##Pantallas auxiliares
    image p_oscura ="#000000"
    image PrologoImg text = Text("Prologo.", size=50) ##muestra un texto desplegable

#Escenarios ###################################
    image ciudad = "images/bg/tokyo_street.jpg"

#Finales  ################################
    image bad_end = "images/ends/game_over.png"
    image medium_end ="images/ends/game_over_2.png"
    image good_end ="images/ends/game_over_3.png"

 #Fechas  ###################################
    image d1_inicio = "images/days/d1.jpg"

## variables del juego

    $ persistent.activarGaleria  #TODO usar variables persistentes y flags
    $ ruta_nuri = 5 ##cada decision tomada sumara a la variable
    $ ruta_diana = 5 # si

    $ ruta_Verdadera = False
    $ ruta_neutral = False ## FIXME:si final neutral => Final True!
    $ ruta_Mala1 = False
    $ ruta_Mala2 = False
    $ ruta_Mala3 =False

    $ decisiones_Ruta = 0

    $ capituloUno = False
    $ capituloDos = False
    $ capituloTres = False
    $ capituloCuatro = False
    $ capituloCinco = False
    $ capituloSeis = False

## MUSICA ####
    define music_casual = "bgm/casual.ogg" ##==> ruta
    define music_amb = "bgm/ambiente.ogg"
    define music_decision = "bgm/decision.ogg"
    define music_paradox= "bgm/paradoxTheme.ogg"
    define music_tension ="bgm/tension.ogg"
