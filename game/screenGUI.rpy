init:
    $ inicio = 0
    $ rango = 5
    $ retraso = 1.0
    $ old_value = None
    screen indicador(): ## BUG: falta animacion de la barra
        hbox:
            xalign 1.0
            ypos 10 ##altura
            vbox:
                text "Estatus" xalign .5
                bar value AnimatedValue(inicio,rango,retraso,old_value) xalign 0.0 xsize 200 ##staticValue
    #Fecha
    screen fecha(dato):### TODO: hacerlo dinamico!!
            hbox:
                xalign 0.0 ypos 10 xpos 10
                hbox:
                    text dato id "dato"

    #Click para continuar
    screen ctc():
        hbox:
            at ctc_apear
            xpos 10 ##xaling .99
            ypos 400 #xalign .99
            text _("(Click para continuar..)")
            
    transform ctc_apear:
        alpha 0,0
        pause 5.0
        linear 0.5 alpha 1.0

    ## animacion para el contrareloj ##############
    # transform alpha_dissolve:
    #     alpha 0.0
    #     linear 0.5 alpha 1.0
    #     on hide:
    #         linear 0.5 alpha 0

    # Timer!!
init:
    python:
        def contrarelojV2(st, at, length = 0.0):
            remaining = length - st
            if remaining > 4.0:
                return Text("%.1f" % remaining, color ="#FFFFFF", size=80), .1
            elif remaining > 0.0:
                return Text("%.1f" % remaining, color ="#FF2D00", size=72), .1
            else:
                return anim.Blink(Text("0.0", color ="#0098FB", size=72)), None

    image contrarelojV2 = DynamicDisplayable(contrarelojV2, length=10.0)

## Extras menu Principal ##################################################################

screen extras:
    tag menu
    add gui.game_menu_background
    vbox:
        ypos 200
        xalign 0.5
        grid 1 3:
            imagebutton: 
                idle "images/resources/galery.png"
                action Show('gallery')
            imagebutton: 
                idle "images/resources/music.png" 
                action Show('music_room')
            textbutton "Volver" action Return()