init python:

    # Step 1. Create the gallery object.
    g = Gallery()
    g.locked_button = "cg/gallocked.png" #miniatura de desbloqueo

    # Step 2. Add buttons and images

    g.button("ending1")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
                                      # $ persistent.unlock_1 = True  => unlock_x x=numero_imagen
    g.image("cg/cg1.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = blinds

    g.button("ending2")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg2.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = blinds

    g.button("ending3")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg3.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve
    
    g.button("ending4")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg4.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve

    g.button("ending5")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg5.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve

    g.button("ending6")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg6.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve

    g.button("endingTrue")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg7.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve

    g.button("cgi_Uno")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg8.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve

    g.button("cgi_Dos")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg9.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve

    g.button("cgi_Tres")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg10.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve

    g.button("cgi_Cuatro")
    g.condition("persistent.unlock_1") # CODIGO para el archivo script.rpy u otro para desbloquear Cgi`s
    g.image("cg/cg11.png","cg/gallunlock.png") ##imagendeFondo, Imagen principal!!
    g.Transition = dissolve

    # Step 3. The gallery screen we use.
screen gallery(pagina =0): ## screens.rpy => line 309

        # Ensure this replaces the main menu.
        tag menu
        textbutton "Musica" action Show("gallery", pagina =0)
        textbutton "Imagen" action Show("gallery", pagina =1)
        textbutton "Escenas" action Show("gallery", pagina =1)
        textbutton "Regresar" action Return()
        # The background
        add "images/resources/whiteBg.png" #imagen de fondo de la galeria .png

        # A grid of buttons.
        grid 6  2:  #24 CGI's 23 + 1 => representa el boton volver

            xfill True
            yfill True

            # Call make_button to show a particular button.
            add g.make_button("ending1", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("ending2", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("ending3", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("ending4", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("ending5", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("ending6", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("endingTrue", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("cgi_Uno", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("cgi_Dos", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("cgi_Tres", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            add g.make_button("cgi_Cuatro", "cg/gallocked.png", xalign=0.5, yalign=0.5)
            # imagenes agregados a la galeria

            # The screen is responsible for returning to the main menu. It could also
            # navigate to other gallery screens.
            textbutton "Volver" action Show("extras") xalign 0.5 yalign 0.5
#
# ####### The button to open up the achievement screen ######FIXME mostrar la pantalla
init:
    default showach_text = False
    screen ach_button: 
        imagebutton:
            xpos 35 ypos 35
            auto "cg/cg1.png"
            hovered ToggleVariable('showach_text')
            unhovered ToggleVariable('showach_text')
            action Jump("ending1")
        if showach_text:
            text "Achievements":
                font "fonts/NotoSans-italic.ttf"
                size 25
                #xpos =45
                #ypos =35

label openscr_ach:
    $ showach_text = False
    show screen achievementdisplay
    return

###### The achievement screen ######

screen achievementdisplay:
    image "images/resources/whiteBg.png" xpos 350 ypos 350
    vpgrid:
        cols 3
        ysize 600
        mousewheel True
        draggable True
        scrollbars "vertical"
### Achievement List Begin ###
# Ach_001 #
        if achievement.has("Logro1"):
            imagebutton:
                auto "images/icons/ach_1.png"
                action NullAction()
        else:
            imagebutton:
                auto "cg/gallocked.png"
                action NullAction()
            # Ach_002 #

        if achievement.has("Logro2"):
            imagebutton:
                auto "images/icons/ach_2.png"
                action NullAction()
        else:
            imagebutton:
                auto "cg/gallocked.png"
                action NullAction()
            ## I want to display the achievement progress here
            ## I have tried this:
            #text "Progress: "+achievement.progress('ach_002', 5)
            ## but it does not work and I don't know what else to do

            textbutton "Close" action Return() xalign 0.5

                # size 20
                #    ypos =1000



# label start:
#
# scene black
# show screen ach_button
#
# mp "I've just completed the first chapter."
# $ get_achievement("ach001", trans=achievement_transform)
#
# mp "Sweet. Now let's pick up an item."
# mp "{i}Player has picked up an item.{/i}"
# $ update_achievement("ach002", 1, trans=achievement_transform)
#
# mp "{i}Player has picked up another item.{/i}"
# $ update_achievement("ach002", 1, trans=achievement_transform)
#
# mp "Let's check on those achievements."
#
# return
