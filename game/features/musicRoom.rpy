init python:

    # Step 1. Create a MusicRoom instance.
    mr = MusicRoom(fadeout=1.0)

    # Step 2. Add music files.
    mr.add("bgm/casual.ogg", always_unlocked=True)
    mr.add("bgm/ambiente.ogg")
    mr.add("bgm/decision.ogg")
    mr.add("bgm/paradoxTheme.ogg")
    mr.add("bgm/tension.ogg")
# Step 3. Create the music room screen.
init:
    screen music_room:
        tag menu
        add "images/resources/phone.png" #imagen de fondo FIXME dar proporcion
        vbox:
            xalign 0.5
            yalign 0.5
            grid 3 3:
            # The buttons that play each track. ·TODO agregar imagen fondo y diseño
                textbutton "1.- Casualidades" action mr.Play("bgm/casual.ogg")
                textbutton "2.- Ambiente" action mr.Play("bgm/ambiente.ogg")
                textbutton "3.- Decisiones" action mr.Play("bgm/decision.ogg")

                textbutton "4.- Paradox In Limine" action mr.Play("bgm/paradoxTheme.ogg")
                textbutton "5.- Tension" action mr.Play("bgm/tension.ogg")
                textbutton "6.- Test 2" action mr.Play("bgm/tension.ogg")

                textbutton "7.- Test 3" action mr.Play("bgm/tension.ogg")
                textbutton "8.- Test 4" action mr.Play("bgm/tension.ogg")
                textbutton "9.- Test 5" action mr.Play("bgm/tension.ogg")

        hbox:
            xalign 0.5
            ypos 400

            textbutton "Avanzar" action mr.Next()
            textbutton "Retroceder" action mr.Previous()
            textbutton "Volver" action Show("extras") xalign 0.5 yalign 0.5

        # TODO: : Musica de inicio al entrar al cuarto de musica??.
        #on "replace" action mr.Play()

        # TODO: :  Restaura el tema del menu principal
        #on "replaced" action Play("music", "track1.ogg")
