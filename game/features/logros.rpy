transform achievement_transform:
    on show:
        xalign .98
        yalign -.3
        linear 0.4 xalign .98 yalign .02
    on hide:
        linear 0.4 xalign 1.9 yalign .02

screen scr_achievement_get(title, a_text, icon, trans=achievement_transform):
    timer 4.0 action Hide("scr_achievement_get")
    window:
        at trans
        background "#000000"
        xalign .98
        yalign .02
        xysize (450, 100)
        hbox:
            vbox:
                spacing 10
                image icon
            vbox:
                xoffset 10
                xsize 330
                text title:
                    size 28
                    id title
                text a_text:
                    size 22
                    id a_text

screen scr_achievement_update(title, a_text, icon, cur_prog, max_prog, trans=achievement_transform):
    timer 2.4 action Hide("scr_achievement_update")
    window:
        at trans
        background "#000000"
        xalign .98
        yalign .02
        xysize (450, 100)

        #

        hbox:
            vbox:
                spacing 10
                image icon
                text "{0}/{1}".format(cur_prog, max_prog):
                    xcenter 0.5
                    ycenter 0.2
            vbox:
                xoffset 10
                xsize 330
                text title:
                    size 28
                    id title
                text a_text:
                    size 22
                    id a_text






init python:
    def get_achievement(ach_id, trans=achievement_transform):
        ach = persistent.achievements_dict[ach_id]
        achievement.grant(ach_id)
        renpy.show_screen(_screen_name='scr_achievement_get', title=ach['title'],
                          a_text=ach['text'], icon=ach['icon'], trans=trans)

    def update_achievement(ach_id, to_add=1, trans=achievement_transform):
        persistent.achievements_dict[ach_id]["cur_prog"] += to_add
        ach = persistent.achievements_dict[ach_id]

        achievement.progress(ach_id, to_add)
        if ach['cur_prog'] > ach['max_prog']:
            persistent.achievements_dict[ach_id]["cur_prog"] = ach['max_prog']
            ach = persistent.achievements_dict[ach_id]

        renpy.show_screen(_screen_name='scr_achievement_update', title=ach['title'], a_text=ach['text'],
                          icon=ach['icon'], cur_prog=ach['cur_prog'], max_prog=ach['max_prog'], trans=trans)





    # Define your achievements here
    if not persistent.achievements_dict:
        persistent.achievements_dict = {"Logro1": {"type": 0,
                                                             "title": "Capitulo 1.",
                                                             "text": "Paradox",
                                                             "icon": "images/icons/ach_1.png" # 96x96 image
                                                             },
                                        "Logro2": {"type": 0,
                                                             "title": "Capitulo 2.",
                                                             "text": "Abismo",
                                                             "icon": "images/icons/ach_2.png"
                                                            },
                                        "Logro3": {"type": 0,
                                                            "title": "Capitulo 3.",
                                                             "text": "Descubrimiento",
                                                             "icon": "images/icons/ach_3.png"
                                                           },
                                        "Logro4": {"type": 0,
                                                             "title": "Capitulo 4.",
                                                             "text": "Descenso",
                                                             "icon": "images/icons/ach_4.png"
                                                              },
                                        "Logro5": {"type": 0,
                                                             "title": "Capitulo 5.",
                                                             "text": "Liberacion",
                                                             "icon": "images/icons/ach_5.png"
                                                             },
                                        "Logro6":{"type": 0,
                                                             "title": "Capitulo 6.",
                                                             "text": "Destino",
                                                             "icon": "images/icons/ach_6.png"
                                                             },
                                        "Logro7": {"type": 0,
                                                             "title": "al 100%",
                                                             "text": "has visto todos los mensajes",
                                                             "icon": "images/icons/ach_7.png"
                                                             },
                                        "Logro8": {"type": 0,
                                                             "title": "Re",
                                                             "text": "has visto todas las lineas alternativas",
                                                             "icon": "images/icons/ach_8.png"
                                                             }
                                        }


        for i, a in persistent.achievements_dict.items():
            if a['type'] == 0:
                achievement.register(i, steam=a['title'])
            if a['type'] == 1:
                achievement.register(i, steam=a['title'], stat_max=a['max_prog'])

                #To implement, use one of these functions:

    # Get_achievement:
    #
    # $ get_achievement(name, trans=achievement_transform) — to award a one-time achievement
    #
    # trans — animation for the achievement
    #
    # Update_achievemnt:
    #
    # $ update_achievement(name, step=1, trans=achievement_transform) — to update an achievement with progress
    #
    # step — how much should be added, by default =1
    #
    # trans — animation for the achievement
    #
    # After that you can use basic functionality.
